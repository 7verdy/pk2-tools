let jsonStats = {};

async function generate() {
    body = {}

    let set = document.getElementById("setInput");
    if (set === undefined) {
        return;
    }

    set = set.value.split(' ')[0].toLowerCase();

    let stats = [document.getElementById("atkInput"), document.getElementById("agiInput"),
        document.getElementById("intInput"), document.getElementById("vitInput")];

    if (stats.includes(undefined)) {
        return;
    }

    stats.forEach((stat) => {
        stat = stat.value;
    });
    body['stats'] = stats;
    

    let setJson = await (await (await fetch(`http://127.0.0.1:7777/set/stats`)).body(body)).json();
    console.log(setJson);
}

function setArmour() {
    armour = document.getElementById("armourInput").value;

}



function hideWeapons() {
    swordDiv = document.getElementById("sword-piece");
    swordDiv.style.display = "none";
    shieldDiv = document.getElementById("shield-piece");
    shieldDiv.style.display = "none";
    daggersDiv = document.getElementById("daggers-piece");
    daggersDiv.style.display = "none";
    heavyDiv = document.getElementById("heavy-piece");
    heavyDiv.style.display = "none";
}

function selectedWeapon() {
    let userPicked = document.getElementById("weapon-type").value;
    let view = document.getElementById("view");
    hideWeapons();
    if (userPicked == 'SnS') {
        swordDiv = document.getElementById("sword-piece");
        swordDiv.style.display = "block";
        shieldDiv = document.getElementById("shield-piece");
        shieldDiv.style.display = "block";
        view.style.height = "30rem";
        console.log('SnS');
    } else if (userPicked == 'Daggers') {
        daggersDiv = document.getElementById("daggers-piece");
        daggersDiv.style.display = "block";
        view.style.height = "20rem";
    } else if (userPicked == 'Heavy-Weapon') {
        heavyDiv = document.getElementById("heavy-piece");
        heavyDiv.style.display = "block";
        view.style.height = "20rem";
    }
}