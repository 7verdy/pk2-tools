const areas = ["Maille", "Pompon", "Caldemount", "Violetfair", "Aldor", "Sagacia"];
const sets = ["Adventurer Set", "Fury Set", "Elastic Set"];
let atk, agi, int, vit;

function updateArea() {
  // TODO: Replace both updateArea and updateSet with a single function that
  //       takes the id of the input field and the array to look in. 
  var x = document.getElementById("areaInput");
  if (x.value == "") return;
  for (let area of areas) {
    if (area.startsWith(x.value)) {
      x.value = area;
    }
  }
}

function updateSet() {
  var x = document.getElementById("setInput");
  if (x.value == "") return;
  for (let set of sets) {
    if (set.startsWith(x.value)) {
      x.value = set;
      fillInArea(x.value);
    }
  }

}

function fillInArea(name) {
  // TODO: Get the sets for each area from the API.
  let mailleSets = [
    "Adventurer Set",
    "Fury Set",
    "Elastic Set"
  ];
  document.getElementById("areaInput").name = name;
  if (mailleSets.includes(name)) {
    document.getElementById("areaInput").value = "Maille";
  }
}

function setStats() {
  var atkField = document.getElementById("atkInput");
  var agiField = document.getElementById("agiInput");
  var intField = document.getElementById("intInput");
  var vitField = document.getElementById("vitInput");
  if (atkField.value !== '') {
    atk = atkField.value;
  }
  if (agiField.value !== '') {
    agi = agiField.value;
  }
  if (intField.value !== '') {
    int = intField.value;
  }
  if (vitField.value !== '') {
    vit = vitField.value;
  }
}

function autocomplete(inp, arr) {
  var currentFocus;

  inp.addEventListener("input", function (e) {
    var a, b, i, val = this.value;
    closeAllLists();
    if (!val) { return false; }
    currentFocus = -1;
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    this.parentNode.appendChild(a);
    for (i = 0; i < arr.length; i++) {
      if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        b = document.createElement("DIV");
        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
        b.innerHTML += arr[i].substr(val.length);
        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
        b.addEventListener("click", function () {
          inp.value = this.getElementsByTagName("input")[0].value;
          closeAllLists();
        });
        a.appendChild(b);
      }
    }
  });

  inp.addEventListener("keydown", function (e) {
    var x = document.getElementById(this.id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 40) {
      currentFocus++;
      addActive(x);
    } else if (e.keyCode == 38) {
      currentFocus--;
      addActive(x);
    } else if (e.keyCode == 13) {
      e.preventDefault();
      if (currentFocus > -1) {
        if (x) x[currentFocus].click();
      }
    }
  });

  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    x[currentFocus].classList.add("autocomplete-active");
  }

  function removeActive(x) {
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }

  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }

  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });
}

autocomplete(document.getElementById("areaInput"), areas);
autocomplete(document.getElementById("armourInput"), sets);